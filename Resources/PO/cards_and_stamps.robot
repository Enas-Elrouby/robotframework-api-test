*** Settings ***
Resource    ../common.robot


*** Keywords ***
Get Cards And Stamps
    [Arguments]       ${client_id}
    ${response} =   GET     ${BASE_URL}${client_id}     expected_status=Anything
    [Return]     ${response}