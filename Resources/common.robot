*** Settings ***
Library  RequestsLibrary
Library  Collections
Library  OperatingSystem
Library  String

*** Variables ***
${BASE_URL}     https://6171294cc20f3a001705fb10.mockapi.io/clients/
${CARDS}        /cards
${STAMPS}       /stamps

*** Keywords ***

Assert Equal
    [Arguments]       ${expected}   ${actual}
    Should Be Equal As Strings    ${expected}    ${actual}


Set Basic header
  ${HEADERS}=    Create Dictionary   accept=application/json, text/plain, */*             Content-Type=application/json    Accept-Encoding=gzip, deflate, br       Accept-Encoding=gzip, deflate, br
  [Return]      ${HEADERS}