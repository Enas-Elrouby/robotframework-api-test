FROM python:3.8.2-slim-buster
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
	gcc g++ musl-dev make && \
    pip install --no-cache-dir robotframework robotframework-extendedrequestslibrary robotframework-faker robotframework-jsonlibrary robotframework-jsonvalidator robotframework-requests robotframework-jsonschemalibrary RESTinstance markdown