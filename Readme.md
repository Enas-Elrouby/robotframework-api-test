### This Repo for testing API service using Robot framework and  to run within gitlab CI pipeline


## To run it locally
######    First install robot framework and robotframework-requests library
    - pip install robotframework
    - pip install robotframework-requests

######    Second run the following command
    - robot Test
    

## Main scenarios
    - Get cards and stamps with valid client id
    - Get cards and stamps with invalid client id

## Provided reports
    - Go to project repo >> Setting >> Pages
    - URL: https://enas-elrouby.gitlab.io/robotframework-api-test
