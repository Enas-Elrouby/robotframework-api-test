*** Settings ***
Resource    ../Resources/PO/cards_and_stamps.robot
Resource    ../Resources/common.robot

*** Test Cases ***
Get Cards And Stamps List By Existing Client Id & Has Promotion And Cards
    [Tags]  Positive
    ${response}    Get Cards And Stamps     1
    Assert Equal    ${response.status_code}     200
    Assert Equal    ${response.json()}[stamps][0][amount]       34398
    Assert Equal    ${response.json()}[stamps][0][promotion]        Efteling
    Assert Equal    ${response.json()}[cards][0][amount_full_cards]     1
    Log to console      Stamps Amount = ${response.json()}[stamps][0][amount]
    Log to console      Promotion = ${response.json()}[stamps][0][promotion]
    Log to console      Amount Full Cards = ${response.json()}[cards][0][amount_full_cards]



Get Cards And Stamps List By Existing Client Id & Has Promotion And No Cards
    [Tags]  Positive
    ${response}    Get Cards And Stamps     12
    Assert Equal    ${response.status_code}     200
    Should Be Empty      ${response.json()}[cards]
    Should Not Be Empty    ${response.json()}[stamps][0]
    Log to console      Stamps Amount = ${response.json()}[stamps][0][amount]
    Log to console      Promotion = ${response.json()}[stamps][0][promotion]



Get Cards And Stamps List By Existing Client Id & Has No Promotion And No Cards
    [Tags]  Positive
    ${response}    Get Cards And Stamps     26
    Assert Equal    ${response.status_code}     200
    Should Be Empty      ${response.json()}[stamps]
    Should Be Empty      ${response.json()}[cards]


Get Cards And Stamps List By Existing Client Id & Has Amount And Promotion
    [Tags]  Positive
    ${response}    Get Cards And Stamps     9
    Assert Equal    ${response.status_code}     200
    Should Be True     """${response.json()}[stamps][0][amount]""" != """"""
    Should Be True     """${response.json()}[stamps][0][promotion]""" != """"""


Get Cards And Stamps List By Non Existing Client Id
    [Tags]  Negative
    ${response}    Get Cards And Stamps     9564
    Assert Equal    ${response.status_code}    404
    Assert Equal    ${response.json()}      Not found


Get Cards And Stamps List By Not Valid Client Id
    [Tags]  Negative
    ${response}    Get Cards And Stamps     @#$6$
    Assert Equal    ${response.status_code}    404
    Assert Equal    ${response.json()}      Not found